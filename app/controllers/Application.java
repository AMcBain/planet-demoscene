package controllers;

import play.cache.Cache;
import play.mvc.Controller;

import models.Data;

public class Application extends Controller
{
    private static final Data EMPTY = new Data();

    public static void index ()
    {
        Data data = Cache.get("data", Data.class);
        if(data == null)
        {
            data = EMPTY;
        }
        render(data);
    }
}
