package extensions;

import java.util.HashMap;
import java.util.Map;

import play.templates.JavaExtensions;

public class PouetExtensions extends JavaExtensions
{
    private static final String PREFIX = "http://content.pouet.net/gfx/types/";

    public static String type (String type)
    {
        return PREFIX + type.replaceAll(" ", "_") + ".gif";
    }
}
