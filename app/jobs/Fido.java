package jobs;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.InputStream;
import java.io.IOException;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import static java.nio.charset.StandardCharsets.UTF_8;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Date;

import models.Data;
import models.DemozooAuthor;
import models.DemozooProduction;
import models.PouetGroup;
import models.PouetProduction;
import models.PouetResult;

import org.apache.commons.io.IOUtils;

import play.cache.Cache;
import play.jobs.Every;
import play.jobs.Job;

@Every("6h")
public class Fido extends Job
{
    private static final int ID = 1433;

    public void doJob ()
    {
        try
        {
            HttpURLConnection connection = connect("http://demozoo.org/api/v1/releasers/" + ID + "/productions/?format=json");

            if (connection.getResponseCode() == 200)
            {
                try (InputStream is = connection.getInputStream())
                {
                    String json = IOUtils.toString(is, UTF_8);

                    Gson gson = new Gson();
                    Type type = new TypeToken<ArrayList<DemozooProduction>>(){}.getType();
                    List<DemozooProduction> productions = gson.fromJson(json, type);

                    // Well this is a "Planet" app. What do you call the person in charge of a planet?
                    DemozooAuthor ruler = null;

                    Set<String> ids = new HashSet<String>();
                    List<PouetProduction> prods = new ArrayList<PouetProduction>();

                    int max = Math.min(productions.size(), 10);
                    for (int i = 0; i < max; i++)
                    {
                        DemozooProduction production = productions.get(i);
                        connection = connect("http://api.pouet.net/v1/search/prod/?q=" + URLEncoder.encode(production.title, "UTF-8"));

                        try (InputStream pis = connection.getInputStream())
                        {
                            json = IOUtils.toString(pis, UTF_8);

                            // Yeah, I know, I could hard code the name but this means I only need one piece of info: the group's ID.
                            if (ruler == null)
                            {
                                for (DemozooAuthor author : production.author_nicks)
                                {
                                    if (author.releaser.id == ID)
                                    {
                                        ruler = author;
                                    }
                                }
                            }

                            PouetResult result = gson.fromJson(json, PouetResult.class);
                            if (result.success)
                            {
                                PouetProduction prod = findProduction(ids, result, ruler, production);
                                if (prod != null)
                                {
                                    prods.add(prod);
                                    ids.add(prod.id);
                                }
                            }
                        }

                        // Try to hammer Pouet less, but still get our data in a timely manner.
                        // Tests on my desktop put the above alone anywhere from 434ms to 1000ms.
                        try
                        {
                            Thread.sleep(15000);
                        }
                        catch (InterruptedException e)
                        {
                            // Ignore... what else are we going to do?
                        }
                    }

                    Collections.sort(prods);

                    Cache.delete("data");
                    Cache.set("data", new Data(prods), "7h");
                }
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    private HttpURLConnection connect (String href) throws IOException
    {
        URL url = new URL(href);
        HttpURLConnection connection = (HttpURLConnection)url.openConnection();
        connection.setRequestProperty("User-Agent", "Planet Slipstream JIRA System Dashboard updater (https://slipstream.asmcbain.net/jira)");
        connection.setRequestProperty("method", "GET");
        connection.setDoInput(true);
        connection.setUseCaches(false);
        connection.setConnectTimeout(3000);
        connection.setReadTimeout(60000);
        connection.connect();
        return connection;
    }

    private PouetProduction findProduction(Set<String> ids, PouetResult result, DemozooAuthor ruler, DemozooProduction production)
    {
        for (String key : result.results.keySet())
        {
            PouetProduction prod = result.results.get(key);

            if (production.title.compareToIgnoreCase(prod.name) == 0 && !ids.contains(prod.id))
            {
                for (PouetGroup group : prod.groups)
                {
                    if (ruler.name.compareToIgnoreCase(group.name) == 0)
                    {
                        return prod;
                    }
                }
            }
        }
        return null;
    }
}
