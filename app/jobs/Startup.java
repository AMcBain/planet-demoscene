package jobs;

import play.jobs.Job;
import play.jobs.OnApplicationStart;

@OnApplicationStart
public class Startup extends Job
{
    public void doJob ()
    {
        new Fido().doJob();
    }
}
