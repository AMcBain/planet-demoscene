package models;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

public class Data implements Serializable
{
    public List<PouetProduction> productions;

    public Data ()
    {
        this.productions = Collections.emptyList();
    }

    public Data (List<PouetProduction> productions)
    {
        this.productions = productions;
    }
}
