package models;

import java.io.Serializable;
import java.util.List;

public class DemozooProduction implements Serializable
{
    public String url;
    public String demozoo_url;
    public int id;
    public String title;
    public List<DemozooAuthor> author_nicks;
    // Don't know what goes inside here. Punt on definition.
    public List<Object> author_affiliation_nicks;
    public String supertype;
    public List<DemozooPlatform> platforms;
    public List<DemozooType> types;
}
