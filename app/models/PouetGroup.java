package models;

import java.io.Serializable;

public class PouetGroup implements Serializable
{
    public String id;
    public String name;
    public String web;
    public String addedUser;
    public String addedDate;
    public String acronym;
}
