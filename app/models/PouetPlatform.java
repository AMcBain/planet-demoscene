package models;

import java.io.Serializable;

public class PouetPlatform implements Serializable
{
    public static final String PLATFORM_ICON_URL = "http://content.pouet.net/gfx/os/";

    public String name;
    public String icon;
    public String slug;

    public String getIcon ()
    {
        return PLATFORM_ICON_URL + icon;
    }
}
