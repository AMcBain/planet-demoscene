package models;

import java.util.List;
import java.util.Map;

import java.io.Serializable;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;

public class PouetProduction implements Comparable<PouetProduction>, Serializable
{
    public String[] types;
    public Map<String, PouetPlatform> platforms;
    public List<PouetPlacing> placings;
    public List<PouetGroup> groups;
    public List<PouetAward> awards;
    public String id;
    public String name;
    public String type;
    public String addedUser;
    public String addedDate;
    public String releaseDate;
    public String voteup;
    public String votepig;
    public String votedown;
    public String voteavg;
    public String download;
    public String party_compo;
    public String party_place;
    public String party_year;
    public PouetParty party;
    public PouetUser addeduser;
    public String commentCount;
    public int cdc;

    public String getReleased ()
    {
        if (party_compo != null)
        {
            return party.name + " " + party_year;
        }
        return releaseDate;
    }

    private static String _getReleaseDate(PouetProduction production)
    {
        if (production.releaseDate == null)
        {
            if (production.party_compo == null)
            {
                return production.addedDate.replace(" ", "T");
            }
            return production.party_year + "-01-01";
        }
        return production.releaseDate;
    }

    public int compareTo(PouetProduction production)
    {
        LocalDate release = new LocalDate(_getReleaseDate(this));
        LocalDate productionRelease = new LocalDate(_getReleaseDate(production));

        if (release.equals(productionRelease))
        {
            return name.compareTo(production.name);
        }
        return productionRelease.compareTo(release);
    }
}
