package models;

import java.io.Serializable;
import java.util.Map;

public class PouetResult implements Serializable
{
    public boolean success;
    public boolean error;
    public Map<String, PouetProduction> results;
}
