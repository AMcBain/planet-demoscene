package models;

import java.io.Serializable;

public class PouetUser implements Serializable
{
    public String id;
    public String nickname;
    public String level;
    public String permissionSubmitItems;
    public String avatar;
    public String glops;
    public String registerDate;
}
